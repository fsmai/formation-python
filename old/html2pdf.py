"""

pip install selenium
apt install phantomjs

"""


from pathlib import Path

from selenium import webdriver 
import selenium 


page = Path("./merged.html").absolute()


def execute(script, args): 
    driver.execute('executePhantomScript', {'script': script, 'args' : args }) 


driver = webdriver.PhantomJS('phantomjs') 


# hack while the python interface lags 
driver.command_executor._commands['executePhantomScript'] = ('POST', '/session/$sessionId/phantom/execute') 

driver.get(f"file://{page}") 

# set page format 
# inside the execution script, webpage is "this" 
pageFormat = (
  '''this.paperSize = {
    format: "A4",
    orientation: "portrait",
    margin: {
      top: "1cm",
      bottom: "1cm",
      left: "2cm",
      right: "2cm"
    }'''
#    ''',
#    footer: {
#      height: "1cm",
#      contents: phantom.callback(function(pageNum) {
#        return "<h1><span style='float:right'>" + pageNum + "</span></h1>";
#      })
#    }'''
    '''};'''
)
execute(pageFormat, []) 

# render current page 
render = f'''this.render("{page.with_suffix('.pdf')}")''' 
execute(render, []) 

