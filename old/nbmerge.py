"""
usage:

fusionne tous les notbooks pointes dans index.ipynb

python nbmerge.py index.ipynb > merged.ipynb
"""

import io
import sys
import re
import os

# from IPython.nbformat import current
from nbformat import current


def merge_notebooks(filenames, ref_dir):
    merged = None
    for fname in filenames:
        with io.open(fname, 'r', encoding='utf-8') as f:
            # nb = current.read(f, 'json')  # DEPRECATED
            nb = current.read(f, 4)

            fname = os.path.normpath(fname)
            localize_ipynb_refs(nb.worksheets[0].cells, fname, ref_dir)
            internalize_ipynb_refs(nb.worksheets[0].cells)

            tag_cell = type(nb.worksheets[0].cells[0])(
                source="<a id=%r></a>" % to_id(fname),
                cell_type='markdown',
                metadata={}
            )
            nb.worksheets[0].cells.insert(0, tag_cell)

        if merged is None:
            merged = nb
        else:
            merged.worksheets[0].cells.extend(nb.worksheets[0].cells)
    merged.metadata.name += "_merged"
    # return current.writes(merged, 'json')  # DEPRECATED
    return current.writes(merged, version=4)


def localize_ipynb_refs(cells, current_file, reference_dir):
    local_dir = os.path.dirname(current_file)

    def repl(match):
        text, fname = match.group(1, 2)
        fname = os.path.relpath(os.path.join(local_dir, fname), reference_dir)
        fname = to_id(fname)
        return "[%s](%s)" % (text, fname)

    for cell in markdown_cell_type(cells):
        cell.source = re.sub("\[(.*?)\]\((.*?\.ipynb)\)", repl, cell.source)


def internalize_ipynb_refs(cells):
    def repl(match):
        return "[%s](#%s)" % match.group(1, 2)
    for cell in markdown_cell_type(cells):
        cell.source = re.sub("\[(.*?)\]\((.*?\.ipynb)\)", repl, cell.source)


def markdown_cell_type(cells):
    for cell in cells:
        if cell.cell_type == 'markdown':
            yield cell


def to_id(fname):
    return str(re.sub(r"/|\\", "_", fname))


def build_file_list_from_index(index_file):
    with io.open(index_file, 'r', encoding='utf-8') as f:
        # index_nb = current.read(f, 'json')  # DEPRECATED
        index_nb = current.read(f, 4)

    fnames = [index_file]
    for cell in markdown_cell_type(index_nb.worksheets[0].cells):
        fnames.extend(
            re.findall("\[.*?\]\((.*?\.ipynb)\)", cell.source)
        )

    return fnames


if __name__ == '__main__':
    try :
        index_file, out_file = sys.argv[1:]
    except Exception:
        print("pas compris :(")
        print("usage: python nbmerge.py index.ipynb merged.ipynb")
    else:
        fnames = build_file_list_from_index(index_file)
        merged_str = merge_notebooks(fnames, os.path.dirname(index_file))
        # print(merged_str)
        # merged_str = unicode(merged_str, errors='replace')
        # merged_str = unicode(merged_str, errors='ignore')
        with io.open(out_file, 'w', encoding='utf-8') as f:
            f.write(merged_str)
