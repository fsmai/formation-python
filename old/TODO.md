On passe la formation à 4 jours pour avoir plus de temps de travaux pratiques.

# idées d'exo supplémentaires

- `devine_le_nombre()`: boucle / input() / if / print(). on peut faire un 2ème exo où c'est le prog qui trouve le nombre.

- un exo filé sur la partie scientifique. A la fin, le prog charge de la donnée avec pandas, calcule grâce à numpy/scipy et fait de la visu avec matplotlib.
On travaille sur le prog au fur et à mesure des chapitres de la partie scientifique.


# nouveaux chapitres

- pandas
- la ligne de commande (après le chapitre sur print())
- pytest (avec les chapitres optionnels ?)
- faire un bon script.
    * `if __name__ == '__main__': main()` -> rendre un script utilisable comme un module
    * `def main(): process(*parse())` -> bien séparer parsing & processing
    * lib pour parsing : `argparse` en standard (alternatives ?)

à mettre dans `Aller plus loin avec Python` ?


# quelques corrections à faire

- [start-python/data-structures.ipynb] supprimer exo matrice 2x2
- [more-python/loop-pythonic.ipynb] reduce() n'est plus builtin
