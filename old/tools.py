import IPython
import os
import glob


def load_all_from(path):
    "load all '*.py' file found in path"
    files = glob.glob('%s/*.py' % path)

    if not files:
        print "No file found in %s" % os.path.abspath(path)
        return

    for filename in files:
        action = 'load %s' % os.path.abspath(filename)
        print action
        IPython.get_ipython().magic(action)
