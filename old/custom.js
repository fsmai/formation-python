/**
 * Copier le code suivant dans
 *    ~/.jupyter/custom/custom.js
 *  ou bien
 *    ~/.ipython/profile_default/static/custom/custom.js
 */


// boutton 'stop' pour exec&below
 $([IPython.events]).on('app_initialized.NotebookApp', function(){
     Jupyter.keyboard_manager.command_shortcuts.add_shortcut(
         'Shift-Ctrl-f',
         {
             handler:  function (event) {
                 IPython.notebook.command_mode();
                 IPython.notebook.execute_cell();
                 IPython.notebook.select_next();
                 return false;
             },
             help : 'run cell, select below',
         }
       );
 });
