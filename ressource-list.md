
# Books, courses
- https://goodresearch.dev/
- https://third-bit.com/py-rse/ ([repo](https://github.com/merely-useful/py-rse))
- https://dabeaz-course.github.io/practical-python/
- https://github.com/dabeaz-course/python-mastery
- https://camdavidsonpilon.github.io/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/


# Interesting peoples
- https://nedbatchelder.com/text/#h_presentations
- https://rhodesmill.org/brandon/
- https://www.dabeaz.com/talks.html

# Some articles, talks, ...

No ordrer, sorry ^^'

- https://www.destroyallsoftware.com/talks/boundaries
- https://seddonym.me/2019/08/03/ioc-techniques/
- [Name Things Once](https://archive.org/details/pyvideo_3792___Name_Things_Once_2) (or [yt](https://www.youtube.com/watch?v=1__lNTlj1_w)) (PyGotham 2015, Jack Diederich)
- [https://davidamos.dev/stop-using-implicit-inputs-and-outputs/](http://web.archive.org/web/20230606172351/https://davidamos.dev/stop-using-implicit-inputs-and-outputs/)
