# Démarrer avec python

## types de bases
* faire une formule
  *  °C -> °F ?

## noms et variables
* idem avec une variable
* déjà proposé

## intro fonctions
* faire la fonction

## conteneurs de base
* une liste de valeurs est fournie
* appliquer la fonction aux premiers éléments de la liste
* (indexation à la main)

## structures de controle
* une fonction de réduction ?
  * mean/sum/autre ?

* une liste de str est fournie
  * transformer la liste
  * puis appliquer la fonction

* une liste de str avec des valeurs invalides est fournie
  * filtrer, transformer la liste
  * appliquer la fonction

# aller plus loin

## Utilisation avancée des chaines de caractères
* utiliser méthodes str pour nettoyage liste str

## Utilisation avancée des indices
* slice de la liste de valeur avant application de fonction

## Lire et écrire des fichiers
* les données sont dans un fichier

## Listes en intension
* filter/ transformer avec des intension

## Fonctions d’ordre supérieur
* faire une fonction apply(data, transformer, reducer)

## Matplotlib
* faire des plots

## Pandas
* refaire en pandas depuis un gros fichier
* faire bien plus...
