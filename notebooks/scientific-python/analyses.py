import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


elems = pd.read_csv("filtre_elem.txt", encoding="iso-8859-1").iloc[:,0]

df = pd.read_csv(
        "analyses.txt", encoding="iso-8859-1", sep="|", parse_dates=["Date prélèvement"]
    ).rename(columns={
        'Date prélèvement': 'date',
        'Code national BSS': 'BSS',
        'Paramètre': 'param',
        "Résultat de l'analyse": "value",
    })
df = df[df.param.isin(elems)][['BSS', 'date', 'param', 'value']]

x = df.set_index(['BSS', 'date', 'param']).value.sort_index()
x = x.loc[~x.index.duplicated(keep='first')]
value = pd.concat({name: x[:,:,name] for name in x.index.levels[2]}, axis=1).reset_index(0)


fig, ax = plt.subplots()
for bss, g in value.groupby("BSS"):
    g.plot(y="Calcium", ax=ax, label=bss)

plt.figure()
sns.scatterplot(data=value, hue='BSS', x="Calcium", y="Chlorures")
plt.show()


def plot_compare_elems(value, x_elem, y_elem):
    subvalue = value[["BSS", x_elem, y_elem]].dropna()
    fig, ax = plt.subplots()
    for bss, gv in subvalue.groupby("BSS"):
        gv.plot(x_elem, y_elem, ax=ax, label=bss, ls='', marker='.', ms=12)
    ax.set_ylabel(y_elem)
