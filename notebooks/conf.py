# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

import sphinx_rtd_theme


# -- Project information -----------------------------------------------------

project = 'Initiation Python'
copyright = '2021, Farid Smaï'
author = 'Farid Smaï'

language = 'fr'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'nbsphinx',
    'sphinx_copybutton',
]

# Add any paths that contain templates here, relative to this directory.
# templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
# exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
exclude_patterns = ['_build', 'template.ipynb']


# -- Options for nbsphinx  ---------------------------------------------------

nbsphinx_execute = 'never'


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

if html_theme == 'sphinx_rtd_theme':
    html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
    html_theme_options = {
        'navigation_depth': 5
    }

html_show_copyright = False

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']


# -- Options for Latex output ------------------------------------------------

latex_logo = r"../img/python-logo-master-v3-TM.png"

latex_toplevel_sectioning = "section"

latex_elements = {
    'fncychap': r'\usepackage[Bjornstrup]{fncychap}',
    'babel': r'\usepackage{babel}',
    'extraclassoptions': 'openany',
    'preamble': r'''
        \setcounter{tocdepth}{2}
        \renewcommand{\sphinxhref}[2]{#2 (\url{#1})}
        %\renewcommand{\sphinxhref}[2]{\href{#1}{#2}\footnote{#1}}
    ''',
}
