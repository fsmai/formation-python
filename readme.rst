A propos
========


Ces supports de formation ne sont que des supports, ils ne sont donc pas vraiment autonomes.
Si leur lecture vous est utile, tant mieux, sinon pensez à vous tourner vers les nombreuses autres ressources disponibles sur le web.

Si vous envisagez de réutiliser ce document pour vos propres besoins, pensez à citer vos sources.


Les supports sont disponibles en différents formats :
 - `jupyter notebook <https://gitlab.com/fsmai/formation-python/-/blob/master/notebooks/index.ipynb>`_
 - `notebooks interactifs sur jupyterlite <https://fsmai.gitlab.io/formation-python/lite/>`_
 - `notebooks interactifs sur Binder <https://mybinder.org/v2/gl/fsmai%2Fformation-python/HEAD?labpath=notebooks%2Findex.ipynb>`_
 - `web <https://fsmai.gitlab.io/formation-python>`_
 - `pdf <https://fsmai.gitlab.io/formation-python/download/initiation-python.pdf>`_

Liens utiles :
 - `dépôt git <https://gitlab.com/fsmai/formation-python>`_
 - `suggestions/corrections <https://gitlab.com/fsmai/formation-python/-/issues>`_
 - `télécharger le dépôt <https://gitlab.com/fsmai/formation-python/-/archive/master/formation-python-master.zip>`_


Contributeurs
-------------

La version initiale de ce document a été produite par Farid Smaï et Théophile Guillon.
