# To build new image with gitlab CI/CD, commit into the branch 'docker'
# (see .gitlab-ci.yml)
#

FROM debian:bullseye

RUN apt-get update

# Latex
RUN apt-get install -y pandoc latexmk texlive texlive-latex-extra texlive-lang-french

# Python
RUN python3 --version
RUN apt-get install -y python3-pip python3-venv

# Sphinx
COPY requirements.txt .
RUN python3 -m pip install -r requirements.txt

# Jupyterlite (need a virtual environment)
COPY jupyterlite-requirements.txt .
RUN python3 -m venv /opt/venv
RUN . /opt/venv/bin/activate && pip install -r jupyterlite-requirements.txt
